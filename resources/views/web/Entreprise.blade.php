@extends('web.layouts.app')

@section('content')
<style>
    /*:after {*/
    /*    content: "";*/
    /*    position: absolute;*/
    /*    top: 0;*/
    /*    left: 0;*/
    /*    height: 100%;*/
    /*    width: 100%;*/
    /*    background: rgba(0, 0, 0, 0);*/
    /*}*/
    .primary_body .home_container .section_heading {
        background: #D8D7DB;
        }
        .navbar{
            background:#d8d7db;
        }
        .primary_header .header_wrapper .main_navigation .navigation_wrapper .nav_menu .nav_menu_wrapper .menu_item .menu_item_href {
    color: #111D5E;}

    .outer-div,
    .inner-div {
        height: 378px;
        max-width: 300px;
        margin: 0 auto;
        position: relative;
    }

    .outer-div {
        perspective: 900px;
        perspective-origin: 50% calc(50% - 18em);
    }

    .inner-div {
        margin: 0 auto;
        border-radius: 5px;
        font-weight: 400;
        color: black;
        font-size: 1rem;
        text-align: center;
        transition: all 0.6s cubic-bezier(0.8, -0.4, 0.2, 1.7);
        transform-style: preserve-3d;
    }

    :hover .social-icon {
        opacity: 1;
        top: 0;
    }

    /*&:hover .front__face-photo,
        &:hover .front__footer {
          opacity: 1;
        }*/


    .outer-div:hover .inner-div {
        transform: rotateY(180deg);
    }

    .front,
    .back {
        position: relative;
        top: 0;
        left: 0;
        backface-visibility: hidden;
    }

    .front {
        cursor: pointer;
        height: 85%;
        background: white;
        backface-visibility: hidden;
        box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
        box-shadow: 0px 1px 15px grey;
        border-radius: 25px;

    }


    .front__face-photo1 {
        position: relative;
        top: 10px;
        height: 120px;
        width: 120px;
        margin: 0 auto;
        border-radius: 50%;

        background-size: contain;
        overflow: hidden;
        /* backface-visibility: hidden;
             transition: all 0.6s cubic-bezier(0.8, -0.4, 0.2, 1.7);
             z-index: 3;*/
    }


    .front__face-photo2 {
        position: relative;
        top: 10px;
        height: 120px;
        width: 120px;
        margin: 0 auto;
        border-radius: 50%;
        background-size: contain;
        overflow: hidden;
        /* backface-visibility: hidden;
             transition: all 0.6s cubic-bezier(0.8, -0.4, 0.2, 1.7);
             z-index: 3;*/
    }


    .front__face-photo3 {
        position: relative;
        top: 10px;
        height: 120px;
        width: 120px;
        margin: 0 auto;
        border-radius: 50%;

        background-size: contain;
        overflow: hidden;
        /* backface-visibility: hidden;
             transition: all 0.6s cubic-bezier(0.8, -0.4, 0.2, 1.7);
             z-index: 3;*/
    }

    .front__text {
        position: relative;
        top: 35px;
        margin: 0 auto;
        font-family: "Montserrat";
        font-size: 18px;
        backface-visibility: hidden;
    }

    .front__text-header {
        font-weight: 700;
        font-family: "Oswald";
        text-transform: uppercase;
        font-size: 20px;
    }

    .front__text-para {
        position: relative;
        top: -5px;
        color: #000;
        font-size: 14px;
        letter-spacing: 0.4px;
        font-weight: 400;
        font-family: "Montserrat", sans-serif;
    }

    .front-icons {
        position: relative;
        top: 0;
        font-size: 14px;
        margin-right: 6px;
        color: gray;
    }

    .front__text-hover {
        position: relative;
        top: 10px;
        font-size: 10px;
        color: red;
        backface-visibility: hidden;

        font-weight: 700;
        text-transform: uppercase;
        letter-spacing: .4px;

        border: 2px solid red;
        padding: 8px 15px;
        border-radius: 30px;

        background: red;
        color: white;
    }


    .back {
        transform: rotateY(180deg);
        position: absolute;
        top: 0;
        left: 0;
        height: 85%;
        width: 100%;
        background-color: #fffefe;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        box-shadow: 0px 1px 15px grey;
        border-radius: 25px;

    }
</style>

<div class="home_container">
    <section class="section_heading mb-3">
        <div class="heading_wrapper container-fluid g-0">
            <div class="row g-0">
                <div class="col-md-6">
                    <div class="row g-0 justify-content-center">
                        <div class="col-auto">
                            <div class="img_wrapper">
                               
                                <img class="heading_img" src="{{asset('img/44.png')}}" alt="" style="   margin-top: -76px;
                                     margin-left: -8px;">
                              
                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-6 p-3">
                    <div class="row g-0 justify-content-start">
                        <div class="col-md-auto">
                           
                            <h2 class="title_heading">TPE - PME</h2>
                            <h3 class="desc_heading">Vos subventions numérique sans conditions </h4>


                                        <div class="search_bloc">
                                            <form class="search">
                                                <input class="search__input" placeholder="Quel type de subvention souhaitez vous" style="">
                                            </form>

                                        </div>
                        </div>
                    </div>

                    <style>
                        .carre {
                            width: 200px;
                            height: 90px;
                            background: white;
                            border-radius: 18px;
                            margin-top: 141px;
                            margin-left: -160px;
                        }
                    </style>
                    <div class="carre">
                        <div class="row">
                            <div class="col" style="display: initial;flex-direction: row;padding: 8px;">
                                <img src="{{asset('img/I.png')}}" style="width: 10%; margin: 0 0 0 30px;" alt="">
                                <span style="margin-left: 4px;font-size: 13px;">Followers</span>
                                <h4 style="text-align: center; margin: 0 120px 0 0;"> {{$result}}K</h4>
                                <p style="text-align: center;font-size: 12px;margin: 0 0px 0 -45px;"><a style="color:#00FF00;">{{$pourcent}}%</a>vs last 7 days</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our_success mb-3">
        <div class="success_wrapper container px-4 g-0">
            <div class="row g-0">

                <div class="col-md-6 py-2">
                    <h5 class="success_txt">Notre succés</h5>
                    <h3 class="success_desc">Peut importe votre secteur d'acitivité nous vous offrons une
                        subvention</h3>
                </div>
                <div class="col-md-6 py-2">
                    <ul class="success_list">
                        <li class="success_item">
                            <h3><span class="item_num">562</span>K</h3>
                            <p class="item_desc">Entreprises</p>
                        </li>
                        <li class="success_item">
                            <h3><span class="item_num">10</span>K</h3>
                            <p class="item_desc">Subventions</p>
                        </li>
                        <li class="success_item">
                            <h3><span class="item_num">200</span>K+ </h3>
                            <p class="item_desc">Adhérants</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="actuality mb-5">
        <div class="float_actions">
            <ul class="actions_content">
                <li class="action_items">
                    <a class="item_href" href="{{route('Entreprise')}}">
                        <i class="ihref_logo"><img width="40%" src="{{asset('/img/Entreprise.png')}}" alt=""></i>
                        <p class="ihref_text">Entreprise</p>
                    </a>
                </li>
                <li class="action_items">
                    <a class="item_href" href="{{route('Agence')}}">
                        <i class="ihref_logo"><img width="40%" src="{{asset('/img/Agence.png')}}"></i>
                        <p class="ihref_text">Agence</p>
                    </a>
                </li>
                <li class="action_items">
                    <a class="item_href" href="{{route('Collectivites')}}">
                        <i class="ihref_logo"> <img width="40%" src="{{asset('/img/Collectivité.png')}}"> </i>
                        <p class="ihref_text">Collectivites</p>
                    </a>
                </li>
            </ul>
        </div>
    </section>
  

   
</div>
<!-- <div>
                           <img src="https://image.flaticon.com/icons/png/512/32/32371.png" alt=""> 
                        </div> -->

<script>
    $(document).ready(function() {
        $('.item_num').counterUp({
            time: 2000
        });
    });
</script>
<script>
    (function($) {
        $('.search').mouseenter(function() {
            $(this).addClass('search--show');
            $(this).removeClass('search--hide');
        });

        $('.search').mouseleave(function() {
            $(this).addClass('search--hide');
            $(this).removeClass('search--show');
        });
    })(jQuery);
</script>







@endsection