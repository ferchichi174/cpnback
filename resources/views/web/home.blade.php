@extends('web.layouts.app')

@section('content')
    <style>
        /*:after {*/
        /*    content: "";*/
        /*    position: absolute;*/
        /*    top: 0;*/
        /*    left: 0;*/
        /*    height: 100%;*/
        /*    width: 100%;*/
        /*    background: rgba(0, 0, 0, 0);*/
        /*}*/


        .outer-div,
        .inner-div {
            height: 378px;
            max-width: 300px;
            margin: 0 auto;
            position: relative;
        }

        .outer-div {
            perspective: 900px;
            perspective-origin: 50% calc(50% - 18em);
        }

        .inner-div {
            margin: 0 auto;
            border-radius: 5px;
            font-weight: 400;
            color: black;
            font-size: 1rem;
            text-align: center;
            transition: all 0.6s cubic-bezier(0.8, -0.4, 0.2, 1.7);
            transform-style: preserve-3d;
        }

        :hover .social-icon {
            opacity: 1;
            top: 0;
        }

        /*&:hover .front__face-photo,
            &:hover .front__footer {
              opacity: 1;
            }*/


        .outer-div:hover .inner-div {
            transform: rotateY(180deg);
        }

        .front,
        .back {
            position: relative;
            top: 0;
            left: 0;
            backface-visibility: hidden;
        }

        .front {
            cursor: pointer;
            height: 85%;
            background: white;
            backface-visibility: hidden;
            box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
            box-shadow: 0px 1px 15px grey;
            border-radius: 25px;

        }


        .front__face-photo1 {
            position: relative;
            top: 10px;
            height: 120px;
            width: 120px;
            margin: 0 auto;
            border-radius: 50%;

            background-size: contain;
            overflow: hidden;
            /* backface-visibility: hidden;
                 transition: all 0.6s cubic-bezier(0.8, -0.4, 0.2, 1.7);
                 z-index: 3;*/
        }


        .front__face-photo2 {
            position: relative;
            top: 10px;
            height: 120px;
            width: 120px;
            margin: 0 auto;
            border-radius: 50%;
            background-size: contain;
            overflow: hidden;
            /* backface-visibility: hidden;
                 transition: all 0.6s cubic-bezier(0.8, -0.4, 0.2, 1.7);
                 z-index: 3;*/
        }


        .front__face-photo3 {
            position: relative;
            top: 10px;
            height: 120px;
            width: 120px;
            margin: 0 auto;
            border-radius: 50%;

            background-size: contain;
            overflow: hidden;
            /* backface-visibility: hidden;
                 transition: all 0.6s cubic-bezier(0.8, -0.4, 0.2, 1.7);
                 z-index: 3;*/
        }

        .front__text {
            position: relative;
            top: 35px;
            margin: 0 auto;
            font-family: "Montserrat";
            font-size: 18px;
            backface-visibility: hidden;
        }

        .front__text-header {
            font-weight: 700;
            font-family: "Oswald";
            text-transform: uppercase;
            font-size: 20px;
        }

        .front__text-para {
            position: relative;
            top: -5px;
            color: #000;
            font-size: 14px;
            letter-spacing: 0.4px;
            font-weight: 400;
            font-family: "Montserrat", sans-serif;
        }

        .front-icons {
            position: relative;
            top: 0;
            font-size: 14px;
            margin-right: 6px;
            color: gray;
        }

        .front__text-hover {
            position: relative;
            top: 10px;
            font-size: 10px;
            color: red;
            backface-visibility: hidden;

            font-weight: 700;
            text-transform: uppercase;
            letter-spacing: .4px;

            border: 2px solid red;
            padding: 8px 15px;
            border-radius: 30px;

            background: red;
            color: white;
        }


        .back {
            transform: rotateY(180deg);
            position: absolute;
            top: 0;
            left: 0;
            height: 85%;
            width: 100%;
            background-color: #fffefe;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
            box-shadow: 0px 1px 15px grey;
            border-radius: 25px;

        }
    </style>

    <div class="home_container">
        <section class="section_heading mb-3">
            <div class="heading_wrapper container-fluid g-0">
                <div class="row g-0">
                    <div class="col-md-6">
                        <div class="row g-0 justify-content-center">
                            <div class="col-auto">
                                <div class="img_wrapper">
                                    @if(auth()->guest())
                                        <img class="heading_img" src="{{asset('img/33.png')}}" alt="" style="   margin-top: -76px;
                                    margin-left: -8px;">
                                    @else
                                        @if(auth()->user()->role=='tpe')
                                            <img class="heading_img" src="{{asset('img/44.png')}}" alt="" style="   margin-top: -76px;
                                     margin-left: -8px;">
                                        @elseif(auth()->user()->role=='age')
                                            <img class="heading_img" src="{{asset('img/22.png')}}" alt="" style=" margin-top: -76px;
                                     margin-left: -8px;">
                                        @elseif(auth()->user()->role=='col')
                                            <img class="heading_img" src="{{asset('img/11.png')}}" alt="" style="   margin-top: -76px;
                                     margin-left: -8px;">
                                        @else
                                            <img class="heading_img" src="{{asset('img/33.png')}}" alt="" style="   margin-top: -76px;
                                      margin-left: -8px;">
                                        @endif
                                    @endif
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6 p-3">
                        <div class="row g-0 justify-content-start">
                            <div class="col-md-auto">
                                @if(auth()->guest())
                                    <h2 class="title_heading">Transition Numérique</h2>
                                    <h4 class="desc_heading">2021</h4>
                                @else
                                    @if(auth()->user()->role=='tpe')
                                        <h2 class="title_heading">TPE - PME</h2>
                                        <h3 class="desc_heading">Vos subventions numérique sans conditions </h4>

                                            @elseif(auth()->user()->role=='age')
                                                <h2 class="title_heading">Agences</h2>
                                                <h3 class="desc_heading">Vos subventions numérique sans conditions </h4>
                                                    @elseif(auth()->user()->role=='col')
                                                        <h2 class="title_heading">Collectivité</h2>
                                                        <h3 class="desc_heading">Vos subventions numérique sans conditions </h4>
                                                            @else
                                                            @endif
                                                            @endif

                                                            <div class="search_bloc">
                                                                <form class="search">
                                                                    <input class="search__input" placeholder="Quel type de subvention souhaitez vous" style="">
                                                                </form>

                                                            </div>
                            </div>
                        </div>

                        <style>
                            .carre {
                                width: 200px;
                                height: 90px;
                                background: white;
                                border-radius: 18px;
                                margin-top: 141px;
                                margin-left: -160px;
                            }
                        </style>
                        <div class="carre">
                            <div class="row">
                                <div class="col" style="display: initial;flex-direction: row;padding: 8px;">
                                    <img src="{{asset('img/I.png')}}" style="width: 10%; margin: 0 0 0 30px;" alt="">
                                    <span style="margin-left: 4px;font-size: 13px;">Followers</span>
                                    <h4 style="text-align: center; margin: 0 120px 0 0;"> {{$result}}K</h4>
                                    <p style="text-align: center;font-size: 12px;margin: 0 0px 0 -45px;"><a style="color:#00FF00;">{{$pourcent}}%</a>vs last 7 days</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="our_success mb-3">
            <div class="success_wrapper container px-4 g-0">
                <div class="row g-0">

                    <div class="col-md-6 py-2">
                        <h5 class="success_txt">Notre succés</h5>
                        <h3 class="success_desc">Peut importe votre secteur d'acitivité nous vous offrons une
                            subvention</h3>
                    </div>
                    <div class="col-md-6 py-2">
                        <ul class="success_list">
                            <li class="success_item">
                                <h3><span class="item_num">562</span>K</h3>
                                <p class="item_desc">Entreprises</p>
                            </li>
                            <li class="success_item">
                                <h3><span class="item_num">10</span>K</h3>
                                <p class="item_desc">Subventions</p>
                            </li>
                            <li class="success_item">
                                <h3><span class="item_num">200</span>K+ </h3>
                                <p class="item_desc">Adhérants</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="actuality mb-5">
            <div class="float_actions">
                <ul class="actions_content">
                    <li class="action_items">
                        <a class="item_href" href="{{route('Entreprise')}}">
                            <i class="ihref_logo"><img width="40%" src="{{asset('/img/Entreprise.png')}}" alt=""></i>
                            <p class="ihref_text">Entreprise</p>
                        </a>
                    </li>
                    <li class="action_items">
                        <a class="item_href" href="{{route('Agence')}}">
                            <i class="ihref_logo"><img width="40%" src="{{asset('/img/Agence.png')}}"></i>
                            <p class="ihref_text">Agence</p>
                        </a>
                    </li>
                    <li class="action_items">
                        <a class="item_href" href="{{route('Collectivites')}}">
                            <i class="ihref_logo"> <img width="40%" src="{{asset('/img/Collectivité.png')}}"> </i>
                            <p class="ihref_text">Collectivites</p>
                        </a>
                    </li>
                </ul>
            </div>
        </section>
        @if(auth()->guest())
            <section class="card-blog mb-5">
                <div class="card_wrapper container px-4 g-0">
                    <div class="row py-3 ">
                        <div class="col-md-4">
                            <div style="background: transparent; color: white; border: none" class="card d-flex flex-column align-items-center justify-content-center">
                                <img style="height:50%;width:50%" src="{{asset('img/agent.png')}}" class="card-img-top" alt="...">
                                <div class="card-body py-0 d-flex flex-column justify-content-center align-items-center">
                                    <h5 class="card-title">Subvention immédiate</h5>
                                    <p class="card-text">Besoin d'un chèque Numérique</p>
                                </div>
                                <div class="card-footer py-0">
                                    <a href="#" style="margin-top: 24px;border-radius: 20px;" class="test-btn3 btn btn-light">Testez Votre éligibilité</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="background: transparent; color: white; border: none" class="card d-flex flex-column align-items-center justify-content-center">
                                <img style="height:50%;width:50%" src="{{asset('img/casque.png')}}" class="card-img-top" alt="...">
                                <div class="card-body py-0 d-flex flex-column justify-content-center align-items-center">
                                    <h5 class="card-title">Accompagnement</h5>
                                    <p class="card-text" style="text-align:center">Des Experts à votre disposition pour digitaliser votre entreprise</p>
                                </div>
                                <div class="card-footer py-0">
                                    <a href="#" style="border-radius: 20px;background-color:#ce1212; text-align:center" class="test-btn3 btn btn-danger">En savoir
                                        plus</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="background: transparent; color: white; border: none" class="card d-flex flex-column align-items-center justify-content-center">
                                <img style="height:50%;width:50%" src="{{asset('img/money.png')}}" class="card-img-top" alt="...">
                                <div class="card-body py-0 d-flex flex-column justify-content-center align-items-center">
                                    <h5 class="card-title">Financement</h5>
                                    <p class="card-text" style="text-align:center">Obtenez le immédiatement sur votre devis ou facture</p>
                                </div>
                                <div class="card-footer py-0">
                                    <a href="#" style="border-radius: 20px;" class="test-btn3 btn btn-light">En savoir
                                        plus</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <section class="text-bloc g-0 mb-5">
                <div class="container px-4">
                    <ul style="margin-left: 15px;" class="text_body">
                        <li style="font-size: 70px;color:#111d5e " class="text_content"></li>
                        <h1 style="margin-top: -75px;margin-left: -17px;color:#111d5e"> Conseils et Accompagnement</h1>
                    </ul>
                    <p style="padding-right: 25rem">Le CPN est un acteur majeur dans la transition digital des entreprises
                        nous subventionnons
                        et accompagnons tous type de projet de développement informatique nous vous orientons au-près
                        d’agence de développement vérifier et désigné.</p>

                </div>
            </section>
            <section class="divider g-0 mb-5">
                <div class="container px-4">
                    <span class="divider_ligne"></span>
                </div>
            </section>


            <section class="second-bloc">

                <div class="container px-4">
                    <h2 style="color:#111d5e">Facilité & Rapidité</h2>
                    <p style="    padding-right: 48rem;">
                        Tous les services du CPN Aide aux entreprises sont destinée aux petites et moyennes entreprises
                        et Start-up.
                    </p>

                    <p style="padding-right: 48rem;">Nous faisons un suivie et donnons accès a notre réseau de
                        partenaires et d’entreprise pour favorisé leurs croissance.
                    </p>
                </div>
                <div class="wavy"></div>
                <div style=" margin: 11px 0px 0px 556px" class="outer-div">
                    <div class="inner-div">
                        <div class="front">
                            <div class="front__face-photo1"><img style="width: 100%;" src="{{asset('img/icone-2.png')}}" alt=""></div>
                            <div class="front__text">
                                <h3 class="front__text-header">Agence</h3>
                                <p class="front__text-para">Grace au Cpn Soyez accompagné au prés d'agence garentie et
                                    referencé</p>

                            </div>
                        </div>
                        <div class="back">
                            <span class="front__text-hover">Hover to Find Me</span>
                        </div>

                    </div>
                </div>
                <div style="margin: -580px 0 0 1065px" class="outer-div">
                    <div class="inner-div">
                        <div class="front">
                            <div class="front__face-photo2"><img style="width: 100%;" src="{{asset('img/icone-1.png')}}" alt=""></div>
                            <div class="front__text">
                                <h3 class="front__text-header">Transformation <br> digitale</h3>
                                <p class="front__text-para">Grace au Cpn Soyez accompagné au prés d'agence garentie et
                                    referencé</p>

                            </div>
                        </div>
                        <div class="back">
                            <span class="front__text-hover">Hover to Find Me</span>
                        </div>

                    </div>
                </div>
                <div style="margin: -12px 0 0 1059px" class="outer-div">
                    <div class="inner-div">
                        <div class="front">
                            <div class="front__face-photo3"> <img style="width: 100%;" src="{{asset('img/icone-3.png')}}" alt="">
                            </div>
                            <div class="front__text">
                                <h3 class="front__text-header">Financement <br> Imédiat</h3>
                                <p class="front__text-para">Grace au Cpn Soyez accompagné au prés d'agence garentie et
                                    referencé</p>

                            </div>
                        </div>
                        <div class="back">
                            <span class="front__text-hover">Hover to Find Me</span>
                        </div>

                    </div>
                </div>


            </section>

            <section class="third-bloc g-0 mb-5">
                <div class="container px-4 text-center">
                    <h1 style="color: #111d5e">Pour obtenir votre subvention</h1>
                    <p style="color: gray" class="subvention-text text-center">
                        Le CPN s'engage à vous mettre en relation avec une agence. <br>
                        Le cabinet vous permet d'obtenir une subvention sur votre <br>
                        développement informatique, et vous met a disposition également <br>
                        sont réseaux d'entreprises et de partenaire international.
                    </p>
                </div>
                <div class="row">
                    <div class="col">
                        <div style="border: 5px solid;    width: 30%;    margin-left: 190px;"></div>
                        <div class="vl" style="border-left: 11px solid black; height: 140px;margin-left: 190px;position: absolute"></div>
                        <img style="margin-left: 190px;height:100%;width:100%" src="{{asset('img/old.jpg')}}" alt="...">
                        <div class="vl" style=" border-left: 11px solid black;  height: 140px;    margin: -130px 0px 0 740px;position: absolute"></div>
                        <div style="border: 5px solid;    width: 10%;    margin-left: 680px;"></div>

                    </div>
                    <div class="col" style="padding-top: 30px; padding-left: 110px;">
                        <div class="third-bloc-border" style="border-radius: 20px;border: 2px solid deepskyblue;width: 90%;padding: 20px;">
                            <h3 style="font-size: 20px;">Economiser</h3>
                            <p>nos subventions sont calculées par rapport à votre investissement quel que soit le poids de votre projet digital. Nos aides varient de 1000 à 10 000 euro de subvention immédiate.</p>
                        </div>
                        <div class="third-bloc-border" style="    border-radius: 20px; border: 2px solid deepskyblue; margin-top: 20px; width: 90%; padding: 20px;">
                            <h3 style="font-size: 20px;">Gagner du temps</h3>
                            <p></p>
                        </div>
                        <div class="third-bloc-border" style="border-radius: 20px;border: 2px solid deepskyblue; margin-top: 20px;width: 90%;padding: 20px;">
                            <h3 style="font-size: 20px;">Service de qualité</h3>
                            <p>Tous nos conseillers détiennent un domaine d’expertise qui leurs est propre et pourront vous accompagner dans la réalisation de vos projets.</p>
                        </div>

                    </div>
                </div>
            </section>

            <section class="fourth-bloc" style="padding-top: 100px">
                <div class="container px-4">
                    <div class="text-center">
                        <h1 style="color: #111d5e">Avis D'entreprises</h1>
                        <p style="color: gray" class="subvention-text text-center">
                            Ils nous ont fait confiance , ont été accompagnés par le CPN et ont réussi à avoir leur <br>
                            subventions
                        </p>
                    </div>
                    <div class="row row-cols-1 row-cols-md-3 g-4 justify-content-center">
                        <div class="col-md-3" style="margin-right: 50px;height: 345px;width: 315px;">
                            <div class="card h-100" style="    box-shadow: 0px 1px 15px grey;border: none;border-radius: 71px 14px 71px 14px;background-color: #ffffff;padding: 40px;">
                                <div class="card-body">

                                    <p class="card-text">"J’ai été accompagné par le CPN et j’ai eu ma subvention, excellent service."</p>
                                    <h4 style="padding: 69px 0 0 26px;font-size: 20px; color:#00BFFF">Justin Rhodes</h4>
                                    <h6 style="padding: 0px 0 0px 54px;font-size: 10px;color:#c7c7c7">Marketing officer</h6>
                                </div>

                            </div>
                            <div style=" position: relative;top: -132px;left: -116px;height: 100px;width: 100px;margin: 0 auto;border-radius: 50%;background-size: contain;overflow: hidden;">
                                <img style="width: 120%;height: 120%" src="{{asset('img/avis 3.jpeg')}}" alt="">


                            </div>
                        </div>
                        <div class="col-md-3" style="margin-right: 50px;height: 345px;width: 315px;">
                            <div class="card h-100" style="    box-shadow: 0px 1px 15px grey;border: none;border-radius: 71px 14px 71px 14px;background-color: #ffffff;padding: 40px;">
                                <div class="card-body">

                                    <p class="card-text">"Rapidité, efficacité et un très bon service client, le CPN m’a aidé à numériser mon entreprise."</p>
                                    <h4 style="padding: 44px 0 0 26px;font-size: 20px; color:#00BFFF">Justin Rhodes</h4>
                                    <h6 style="padding: 0px 0 0px 54px;font-size: 10px;color:#c7c7c7">Marketing officer</h6>
                                </div>
                            </div>

                            <div style=" position: relative; top: -132px;left: -116px;height: 100px;width: 100px;margin: 0 auto;border-radius: 50%; background-size: contain;overflow: hidden;">
                                <img style="width: 120%;height: 120%" src="{{asset('img/avis 1.jpeg')}}" alt="">
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-right: 50px;height: 345px;width: 315px;">
                            <div class="card h-100" style="    box-shadow: 0px 1px 15px grey;border: none;border-radius: 71px 14px 71px 14px;background-color: #ffffff;padding: 40px;">
                                <div class="card-body">

                                    <p class="card-text" style="margin-right: -43px;">"Grâce à la digitalisation de mon entreprise j’ai pu augmenter mon chiffre d’affaire, et c’est grâce au CPN que j’ai pu être bien accompagné et conseillé." </p>
                                    <h4 style="padding: 22px 0 0px 29px;font-size: 20px;color:#00BFFF">Justin Rhodes</h4>
                                    <h6 style="padding: 0px 0 0px 54px;font-size: 10px;color:#c7c7c7">Marketing officer</h6>
                                </div>
                            </div>
                            <div style=" position: relative;top: -132px;left: -116px;height: 100px;width: 100px;margin: 0 auto;border-radius: 50%;background-size: contain;overflow: hidden;">
                                <img style="width: 120%;height: 120%" src="{{asset('img/avis 2.jpeg')}}" alt="">


                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </div>
    <!-- <div>
                               <img src="https://image.flaticon.com/icons/png/512/32/32371.png" alt="">
                            </div> -->
    <section class="point g-0 mb-5">
        @if(Session::has('success'))
            <div class="alert alert-success" style="text-align: center;" role="alert">
                {{Session::get('success')}}
            </div>
        @endif
        {{--        <form action="{{route('send_comment')}}" method="post">--}}
        {{--            @csrf--}}
        {{--            <div class="container px-4" style="margin-bottom: 20px;">--}}
        {{--                <textarea class="form-control" placeholder="Commentaire..." name="comment" id="" cols="30" rows="5" style="    width: 60%; border-radius: 8px;border: 2px solid #137feb;margin: auto;margin-top: 50px;"></textarea>--}}
        {{--            </div>--}}
        {{--            <div class="btn_post">--}}
        {{--                <button type="submit" style="text-decoration:auto ; border: none;background: red;border-radius: 25px;color: white; margin-left: 910px;padding: 5px 15px;">Poster</button>--}}
        {{--            </div>--}}
        {{--        </form>--}}
    </section>
    <script>
        $(document).ready(function() {
            $('.item_num').counterUp({
                time: 2000
            });
        });
    </script>
    <script>
        (function($) {
            $('.search').mouseenter(function() {
                $(this).addClass('search--show');
                $(this).removeClass('search--hide');
            });

            $('.search').mouseleave(function() {
                $(this).addClass('search--hide');
                $(this).removeClass('search--show');
            });
        })(jQuery);
    </script>
    @else

        <section class="divider g-0 mb-5">
            <style>
                .main {
                    width: 800px;
                    height: 350px;
                    padding: 3em;
                    margin-left: 25%;


                }

                .texte {
                    width: 800px;
                    height: 350px;
                    padding: 3em;
                    margin-left: 25%;


                }
            </style>



            <div class="main">
                <script src="cmap/france-map.js"></script>
                <script>
                    francefree();
                </script>
            </div>

        </section>

        <section class=" g-0 mb-5">
            <div class="container px-4">
                <h3 style=" margin-left: -1025px;
            color: #111D5E;">Choisir ma région</h3>
            </div>
        </section>

        <section class="divider g-0 mb-5">
            <div class="container px-4">
                <span class="divider_ligne"></span>
            </div>
        </section>
        <section class=" g-0 mb-5">
            <div class="container px-4">
                <h3 style="margin-left: -1150px;
           color: #111D5E;"> Actualite </h3>
            </div>
        </section>
        <div class="row mb-5 g-4 justify-content-center">

            @foreach($articles as $a)

                <div class="col-md-4 col-lg-4 col-xl-4" style="width: 440px;">
                    <div style="overflow: hidden " class="act-wrapper shadow rounded-3 bg-white">
                        <div style="height: 150px;" class="act-img">
                            <img style="width: 100%; height:100%; object-fit:cover" src="{{$a->link}}" alt="actualite" />
                        </div>
                        <div style="height: 190px;" class="act-content p-2">
                            <div class="act-title">
                                <h5> {{$a->title}}| </h5>
                            </div>

                            <div class="act-desc">
                                <p class="m-0">{{$a->desc}}</p>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach

        </div>

        <section class=" g-0 mb-5">
            <div class="container px-4">
                <h3 style=" margin-left: -1150px;
             color: #111D5E;"> Nous proposons </h3>
            </div>
        </section>

        <div class="row mb-5 g-4 justify-content-center">
            <div class="col-md-4 col-lg-4 col-xl-4">
                <div style="overflow: hidden;background: linear-gradient(white,gainsboro);" class="act-wrapper shadow rounded-3 bg-white">

                    <div style="height: 190px;" class="act-content p-2">
                        <div class="act-title">
                            <h5> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </h5>
                        </div>
                        <div class="act-title">
                            <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </h6>
                        </div>
                        <div class="act-desc">
                            <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xl-4">
                <div style="overflow: hidden;background: linear-gradient(white,gainsboro);" class="act-wrapper shadow rounded-3 bg-white">

                    <div style="height: 190px;" class="act-content p-2">
                        <div class="act-title">
                            <h5> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </h5>
                        </div>
                        <div class="act-title">
                            <h6> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </h6>
                        </div>
                        <div class="act-desc">
                            <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xl-4">
                <div style="overflow: hidden;background: linear-gradient(white,gainsboro);" class="act-wrapper shadow rounded-3 bg-white">

                    <div style="height: 190px;" class="act-content p-2">
                        <div class="act-title">
                            <h5> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </h5>
                        </div>
                        <div class="act-title">
                            <h6> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </h6>
                        </div>
                        <div class="act-desc">
                            <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </section>
    @endif
@endsection
