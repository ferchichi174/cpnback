import { TestBed } from '@angular/core/testing';

import { TestEligibiliteService } from './test-eligibilite.service';

describe('TestEligibiliteService', () => {
  let service: TestEligibiliteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestEligibiliteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
