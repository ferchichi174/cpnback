import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/app/baseUrl';
import { Contact } from 'src/app/models/crm-models/contact';
const headers = new HttpHeaders();


headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
@Injectable({
  providedIn: 'root'
})
export class RepertoirService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  /******************************repertoire api ************************/

  public getContactIdUser(id:string): Observable<any>{
    return this.http.get<any>(baseUrl+"/api/contacts/"+id)
  }

  public saveContact(form:Contact): Observable<any>{
    console.log('contact',form)
    return this.http.post<Contact>(baseUrl+"/api/test/contact/save",form)
  }


  /***************************lead contact api **********************/
  public getLeadContactIdUser(id:string): Observable<any[]>{
    return this.http.get<any[]>(baseUrl+"/api/leads/contacts/"+id)
  }

  
  /*************************** Event api **********************/
  public getEvent(): Observable<any[]>{
    return this.http.get<any[]>(baseUrl+"/api/calendar/events/get")
  }
}
