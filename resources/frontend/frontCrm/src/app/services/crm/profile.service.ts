import { Injectable } from '@angular/core';
import {  Profile} from '../../models/crm-models/profile';
import { baseUrl } from '../../baseUrl';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const headers = new HttpHeaders();


headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  
  public getUser(id:string): Observable<any>{
    return this.http.get<any>(baseUrl+"/api/sign/in/"+id)
  }

  public updateUser(form:Profile,id:string): Observable<any>{
    return this.http.post<Profile>(baseUrl+""+id,form)
  }
}
