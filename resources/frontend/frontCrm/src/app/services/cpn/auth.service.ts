import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { baseUrl } from '../../baseUrl';

const headers = new HttpHeaders();


headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  
  register(form): Observable<any> {
    return this.http.post(baseUrl + '/api/inscription', form,  { withCredentials: false })
  }

  
  login(form): Observable<any> {
    return this.http.post(baseUrl + '/api/login', form,  { withCredentials: false });
  }
}
