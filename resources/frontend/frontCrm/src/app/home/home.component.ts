import { Component, OnInit } from '@angular/core';
declare let $ :any;
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private tokenStorage: TokenStorageService,private route: ActivatedRoute) { }

  ngOnInit(): void {

     
    $(document).mousemove(function(e) {
      $( '#info-box').css('top', e.pageY - $( '#info-box').height() - 30);
      $( '#info-box').css('left', e.pageX - ($( '#info-box').width()) / 2);
    }).mouseover();
    
  
      $('.search').mouseenter(function() {
          $(this).addClass('search--show');
          $(this).removeClass('search--hide');
      });
  
      $('.search').mouseleave(function() {
          $(this).addClass('search--hide');
          $(this).removeClass('search--show');
      });
  
      $(document).ready(function() {
        $('.item_num').counterUp({
            time: 2000
        });
    });
  
  
    
  }

 

}
