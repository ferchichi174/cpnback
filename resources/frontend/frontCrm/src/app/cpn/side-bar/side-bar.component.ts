import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $('.float_actions .actions_content .action_items:hover:after').each(function() {
      var link = $(this).html();
      $(this).content().wrap('<a href="example.com/script.php"></a>');
    });
  }

}
