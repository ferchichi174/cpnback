import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from 'src/app/services/cpn/auth.service'
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
registerGroup:FormGroup
  constructor(private auth:AuthService, private fb:FormBuilder) { 
     this.registerGroup = this.fb.group({
      type: ['agence'],
      firstname: ['',[ Validators.required]],
      lastname: ['',[ Validators.required]],
      email: ['',[ Validators.required]],
      password: ['', [Validators.required]],
      password_confirmed: ['', [Validators.required]],

    });
  }
  onSubmit(){
    const formData = new FormData();
    formData.append('type', this.registerGroup.get('type').value );
    formData.append(   'firstname', this.registerGroup.get('firstname').value);
    formData.append( 'lastname', this.registerGroup.get('lastname').value);
    formData.append( 'email', this.registerGroup.get('email').value );
    formData.append('password', this.registerGroup.get('password').value);
    formData.append('password_confirmed', this.registerGroup.get('password_confirmed').value );
   
          
      
   
    console.log('rquet',this.registerGroup.value)

  this.auth.register( formData).subscribe(res=>{
    console.log('rquet',res)
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
    })
  })

}
  ngOnInit(): void {
  }

}
