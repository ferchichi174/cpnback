import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from 'src/app/services/cpn/auth.service'
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
  LoginForm: FormGroup;

  constructor(private fb: FormBuilder,private auth:AuthService, private tokenStorage:TokenStorageService) { 
    this.LoginForm = this.fb.group({
      email: [null,[ Validators.required]],
      password: ['', [Validators.required]],

    })
  }

  ngOnInit(): void {
  }


  /************************login *************************/
  onSubmit(){
    const formData = new FormData();
    formData.append( 'email', this.LoginForm.get('email').value );
    formData.append('password', this.LoginForm.get('password').value);
    this.auth.login(formData).subscribe(data=>{
      console.log('rquet',data.data.token)
      this.tokenStorage.saveToken(data.data.token);
      this.tokenStorage.saveUser(data.data.user);
      location.href='/cpn/Home'
    })

  }
}
