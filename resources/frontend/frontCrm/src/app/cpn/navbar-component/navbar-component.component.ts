import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-navbar-component',
  templateUrl: './navbar-component.component.html',
  styleUrls: ['./navbar-component.component.css']
})
export class NavbarComponentComponent implements OnInit {
  token:any
  user:any=null
  connect=false

      /******************************life cycle *************************/
  constructor(private tokenStorage: TokenStorageService,private route: ActivatedRoute) { 

  }

  ngOnInit(): void {
   
    
  }

  redirectTo(to){
    location.href=to
  }
  logout() {
    this.tokenStorage.signOut();
    console.log("singout",this.tokenStorage.getUser());
    this.connect=false
    location.href = '/cpn/Connexion';
}

}
