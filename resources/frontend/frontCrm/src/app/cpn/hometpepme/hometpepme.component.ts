import { Content } from '@angular/compiler/src/render3/r3_ast';
import { Component, OnInit } from '@angular/core';
declare let $: any;

@Component({
  selector: 'app-hometpepme',
  templateUrl: './hometpepme.component.html',
  styleUrls: ['./hometpepme.component.css']
})
export class HometpepmeComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {
    $(".primary_body .float_actions .actions_content .action_items:hover:after").each(function(e) {
      var link=$(this).html()
      $(this).contents().wrap('<a href="/test"></a>')
      
     // console.log('hover', $( '#info-box').html($(this).data('info')))
    });

    $('.search').mouseenter(function() {
      $(this).addClass('search--show');
      $(this).removeClass('search--hide');
  });

  $('.search').mouseleave(function() {
      $(this).addClass('search--hide');
      $(this).removeClass('search--show');
  });

  $(document).ready(function() {
    $('.item_num').counterUp({
        time: 2000
    });
});
  }

}
