import { Component ,HostBinding} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Profile } from 'src/app/models/crm-models/profile';

@Component({
  selector: 'app-sid-bar',
  templateUrl: './sid-bar.component.html',
  styleUrls: ['./sid-bar.component.css'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({transform: 'rotate(0deg)'})),
      state('expanded1', style({transform: 'rotate(180deg)'})),
      transition('expanded1 <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
      state('expanded2', style({transform: 'rotate(180deg)'})),
      transition('expanded2 <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ]
})
export class SidBarComponent {
  /******************************all variable *************************/
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
    expanded1:boolean=false
    expanded2:boolean=false
    @HostBinding('attr.aria-expanded') ariaExpanded1 = this.expanded1;
    @HostBinding('attr.aria-expanded') ariaExpanded2 = this.expanded2;
    onItemSelected1() {
      this.expanded1 = !this.expanded1; 
         }
    onItemSelected2() {
    this.expanded2 = !this.expanded2; 
        }         

  token:any
  profile:Profile;

    /******************************life cycle *************************/
  constructor(private breakpointObserver: BreakpointObserver,private tokenStorage: TokenStorageService, ) {
    this.token=this.tokenStorage.getUser()
    this.profile=JSON.parse(this.token)
  }


  /******************************logout *************************/
  logout() {
    this.tokenStorage.signOut();
    console.log("singout",this.tokenStorage.getUser());
    location.href = '/login';
} 
}
