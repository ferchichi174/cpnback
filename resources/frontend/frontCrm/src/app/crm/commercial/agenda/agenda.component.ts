import { Component, OnInit } from '@angular/core';
import { CalendarOptions, DateSelectArg, EventClickArg, EventApi } from '@fullcalendar/angular';
import { RepertoirService } from 'src/app/services/crm/repertoir.service';
declare let $: any;

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
      /*************************all variable ***********************/
  calendarOption :any
  events:any[]
  eventCalendrier:Array<any>=[]
  event:any

      /*************************life cycle ***********************/
  constructor(private repertoirService:RepertoirService) { }

  ngOnInit(): void {
    this.repertoirService.getEvent().subscribe(res=>{
      this.events= res  
      for(var value in  res){
         this.events= res[value].map(data=>{return data})       
      }
      for(var value in  this.events){
        this.eventCalendrier.push({ id:this.events[value].id,title:this.events[value].title, start:this.events[value].date,
        color:this.events[value].color});
      }

    
  
     
  this.calendarOption = {
    customButtons: {
      myCustomButton: {
        text: 'custom!',
        click: function () {
          alert('clicked the custom button!');
        }
      }
    },
    locale:"fr",
    initialView: 'dayGridMonth',
    //initialEvents: INITIAL_EVENTS, // alternatively, use the events setting to fetch from a feed
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    droppable: false,
    displayEventTime: true,
    disableDragging:false,
    timeZone: 'UTC',
    refetchResourcesOnNavigate: true,


    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,dayGridWeek,dayGridDay'
    },
    dayMaxEvents: true,
    events:  this.eventCalendrier,
    dateClick: this.handleDateClick.bind(this),
      eventClick: this.handleEventClick.bind(this),
      eventDragStop: this.handleEventDragStop.bind(this)
}

})
  }
  

 
      /************************* date click ***********************/
  handleDateClick(arg) {
    console.log('DateClick',arg);
  }

      /************************* detail event ***********************/
  handleEventClick(arg) {
   this.events.filter(data=>{if(data.id==arg.event.id) return  this.event=data})
   console.log('event',  this.event);
   $('#myModal').appendTo("body").modal('show')
  }

      /************************* exit event  ***********************/
  handleEventDragStop(arg) {
    console.log('EventDragStop',arg);
  }



}
