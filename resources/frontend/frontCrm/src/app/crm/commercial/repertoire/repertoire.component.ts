import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
declare let $: any;
import {Contact} from 'src/app/models/crm-models/contact'
import {RepertoirService} from 'src/app/services/crm/repertoir.service'
import { TokenStorageService } from 'src/app/services/token-storage.service';


@Component({
  selector: 'app-repertoire',
  templateUrl: './repertoire.component.html',
  styleUrls: ['./repertoire.component.css']
})
export class RepertoireComponent implements OnInit {
  /*************************all variable ***********************/
  displayedColumns: string[] = ['CP', 'Nom', 'Prénom', 'Téléphone', 'Email', 'Status', 'Actions'];
  dataSource: MatTableDataSource<Contact>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  contactFormGroup: FormGroup;
  companiesFormGroup: FormGroup;
  investmentFormGroup: FormGroup;
  addressFormGroup: FormGroup;
  developmentFormGroup: FormGroup;
  token:any;
  contacts:[Contact]
  get fContact() { return this.contactFormGroup.controls; }
  get fCompany() { return this.companiesFormGroup.controls; }
  get fInvestment() { return this.investmentFormGroup.controls; }
  get fAdress() { return this.addressFormGroup.controls; }
  get fDevelopment() { return this.developmentFormGroup.controls; }

    /*************************life cycle ***********************/
  constructor(private formBuilder: FormBuilder,private repertoirService:RepertoirService,private tokenStorage: TokenStorageService) {
 
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource();
    
    /**********************contact *********************/
    this.contactFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phoneContact: ['', [Validators.required]],
      position: ['', [Validators.required]],
      });

  
      /**********************companies *********************/
      this.companiesFormGroup = this.formBuilder.group({
        activity: ['', [Validators.required]],
        name: ['', [Validators.required]],
        status: ['', [Validators.required]],
        salaries: ['', [Validators.required]],
        siret: ['', [Validators.required]],
        siren: ['', [Validators.required]],
        naf: ['', [Validators.required]],
        phoneCompany: ['', [Validators.required]],
        turnover: ['', [Validators.required]],
        lastTurnover: ['', [Validators.required]],
        help: ['', [Validators.required]],
      })

      /**********************investment *********************/
      this.investmentFormGroup = this.formBuilder.group({
        service: ['', [Validators.required]],
        transitions: ['', [Validators.required]],
        budget: ['', [Validators.required]],
      })

      /**********************address *********************/
      this.addressFormGroup = this.formBuilder.group({
        line: ['', [Validators.required]],
        region: ['', [Validators.required]],
        city: ['', [Validators.required]],
        zipcode: ['', [Validators.required]],
        departement: ['', [Validators.required]],
        country: ['', [Validators.required]],
      })

      /**********************development *********************/
      this.developmentFormGroup = this.formBuilder.group({
      haveWebsite: ['', [Validators.required]],
      websiteType: ['', [Validators.required]],
      websiteValue: ['', [Validators.required]],
      websiteLink: ['', [Validators.required]],
      websiteDate: ['', [Validators.required]],
      haveCrm: ['', [Validators.required]],
      crmType: ['', [Validators.required]],
      crmDev: ['', [Validators.required]],
      crmName: ['', [Validators.required]],
      erpName: ['', [Validators.required]],
      crmDate: ['', [Validators.required]],
      agencyName: ['', [Validators.required]],
      })
  }

  

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.token=JSON.parse(this.tokenStorage.getUser())

    this.repertoirService.getContactIdUser(this.token.id).subscribe(res=>{     
         this.contacts=res
         console.log("contact",res)
    })
  
  }

  /*************************** add contact ***********************/
  onSubmit() {
 /*   if(this.ContactForm.value){
      let formContact:any={
       "contacts":{ 
        "firstName": this.ContactForm.get("firstName").value,
        "lastName": this.ContactForm.get("lastName").value,
        "email":this.ContactForm.get("email").value,
        "phone":this.ContactForm.get("phone").value,
        "position":this.ContactForm.get("position").value
        },
         "companies":{
         "activity":this.ContactForm.get("activity").value,
         "name":this.ContactForm.get("name").value,
         "status":this.ContactForm.get("status").value,
         "salaries":this.ContactForm.get("salaries").value,
         "siret":this.ContactForm.get("siret").value,
         "siren":this.ContactForm.get("siren").value,
         "naf":this.ContactForm.get("naf").value,
         "phone":this.ContactForm.get("phoneCompany").value,
         "turnover":this.ContactForm.get("turnover").value,
         "lastTurnover":this.ContactForm.get("lastTurnover").value,
         "help":this.ContactForm.get("help").value   
        },
         "investment":{
            "service":this.ContactForm.get("service").value,
            "transitions":"",
            "budget":this.ContactForm.get("budget").value
        },
        "address":{
            "line":this.ContactForm.get("line").value,
            "region":this.ContactForm.get("region").value,
            "city":this.ContactForm.get("city").value,
            "zipcode":this.ContactForm.get("zipcode").value,
            "departement":this.ContactForm.get("departement").value,
            "country":this.ContactForm.get("country").value
        },
        "development":{
            "haveWebsite":this.ContactForm.get("haveWebsite").value,
            "websiteType":this.ContactForm.get("websiteType").value,
            "websiteValue":this.ContactForm.get("websiteValue").value,
            "websiteLink":this.ContactForm.get("websiteLink").value,
            "websiteDate":this.ContactForm.get("websiteDate").value,
            "haveCrm":this.ContactForm.get("haveCrm").value,
            "crmType":this.ContactForm.get("crmType").value,
            "crmDev":this.ContactForm.get("crmDev").value,
            "crmName":this.ContactForm.get("crmName").value,
            "erpName":this.ContactForm.get("erpName").value,
            "crmDate":this.ContactForm.get("crmDate").value,
            "agencyName":this.ContactForm.get("agencyName").value
        }
      }
      this.repertoirService.saveContact(formContact).subscribe(res=>{
         console.log('data send',res)
      })
     }*/
  }

  openModal(){
    $('#myModalrepertoir').appendTo("body").modal('show')
   }
  
  /*************************** filtre  ***********************/
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
