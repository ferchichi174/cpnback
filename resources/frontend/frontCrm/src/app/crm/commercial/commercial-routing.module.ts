import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommercialComponent } from './commercial.component';
import { RepertoireComponent } from './repertoire/repertoire.component';
import { LeadContactComponent } from './lead-contact/lead-contact.component';
import { AgendaComponent } from './agenda/agenda.component';

const routes: Routes = [{ path: '', component: CommercialComponent
 
,children:
[
  { path: 'agendaC', component: AgendaComponent },
  { path: 'repertoir', component: RepertoireComponent },
  { path: 'leadContact', component: LeadContactComponent },
]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommercialRoutingModule { }
