import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {Lead} from 'src/app/models/crm-models/lead'
import { RepertoirService } from 'src/app/services/crm/repertoir.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-lead-contact',
  templateUrl: './lead-contact.component.html',
  styleUrls: ['./lead-contact.component.css']
})
export class LeadContactComponent implements OnInit {

  displayedColumns: string[] = ['ID', 'Nom', 'Prénom', 'entreprise' , 'Téléphone', 'Email', 'Test' , 'Confirmer', 'Actions'];
  dataSource: MatTableDataSource<Lead>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  token:any;
  contacts:Lead[]

    /*************************life cycle ***********************/
  constructor(private repertoirService:RepertoirService,private tokenStorage: TokenStorageService) {
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource();
  }

  

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.token=JSON.parse(this.tokenStorage.getUser())
    console.log("contact",this.token)
    this.repertoirService.getLeadContactIdUser(this.token.id).subscribe(res=>{     
         this.contacts=res
         console.log("contact",res)
    })
  
  }



 
  
  /*************************** filtre  ***********************/
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

