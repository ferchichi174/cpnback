import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {Client} from 'src/app/models/crm-models/client'
import {ClientService  } from 'src/app/services/crm/client.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

    /*************************all variable ***********************/
  displayedColumns: string[] = ['ID', 'Nom', 'Prénom', 'entreprise' , 'Téléphone', 'Email', 'Test' , 'Confirmer', 'Actions'];
  dataSource: MatTableDataSource<Client>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  token:any;
  clients:Client[]=[]

    /*************************life cycle ***********************/
  constructor(private clientService:ClientService,private tokenStorage: TokenStorageService) {
    // Assign the data to the data source for the table to render
  }

  

  ngOnInit() {
 
  //  this.token=JSON.parse(this.tokenStorage.getUser())
 
    this.clientService.getClient().subscribe((res:any) =>{    
         this.clients=res.leads
         console.log('data',this.clients)
         this.dataSource = new MatTableDataSource(this.clients);
         this.dataSource.paginator = this.paginator;
         this.dataSource.sort = this.sort;
    })
  
  }



 
  
  /*************************** filtre  ***********************/
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
