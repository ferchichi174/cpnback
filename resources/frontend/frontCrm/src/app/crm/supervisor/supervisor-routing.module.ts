import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SupervisorComponent } from './supervisor.component';
import { ClientComponent } from './client/client.component';
import { AgendaComponent } from './agenda/agenda.component';


const routes: Routes = [{ path: '', component: SupervisorComponent

,children:
[
  { path: 'client', component: ClientComponent },
  { path: 'agendaS', component: AgendaComponent },
]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupervisorRoutingModule { }
