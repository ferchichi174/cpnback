import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Profile } from 'src/app/models/crm-models/profile';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import {ProfileService} from '../../services/crm/profile.service'
declare let $: any;


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

/********************** all variable *************/
  ModifierProfile: FormGroup;
  id :string ="13";
  profile:Profile;
  token:any;

  /**********************life Cycle ***********************/
    //Add user form actions
    get f() { return this.ModifierProfile.controls; }

  constructor(private formBuilder: FormBuilder,private profileService:ProfileService,private tokenStorage: TokenStorageService, ) {
    this.ModifierProfile = this.formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      adresse: ['', [Validators.required]],
      telephone: ['', [Validators.required]],
      compte: ['', [Validators.required]],
      });
  }
 
 

  ngOnInit(): void {
    this.token=this.tokenStorage.getUser()
    this.profile=JSON.parse(this.token)
   console.log('profile',this.profile)
   /* this.profileService.getUser(this.id).subscribe(res=>{     
    })*/

  }

 openModal(){
  $('#myModal').appendTo("body").modal('show')
 }

 /********************************update profile *****************************/
 onSubmit() {
  if(this.ModifierProfile.value()){
   this.profileService.updateUser(this.ModifierProfile.value(),this.id).subscribe(res=>{
      console.log('data send',res)
   })
  }
}
 
}
