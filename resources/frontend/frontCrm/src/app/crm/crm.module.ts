/**************************librery *******************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MultiSelectAllModule} from "@syncfusion/ej2-angular-dropdowns";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

/**************************component *******************/
import { CrmRoutingModule } from './crm-routing.module';
import { CrmComponent } from './crm.component';
import { SidBarComponent } from './sid-bar/sid-bar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ProfileComponent } from './profile/profile.component';
import { MaterialModule } from '../material-module';
import { TestEligibiliteComponent } from './test-eligibilite/test-eligibilite.component';

import { CountdownModule } from 'ngx-countdown';
import { NgSelectModule } from '@ng-select/ng-select';

import { MapFrenchComponent } from './map-french/map-french.component';

@NgModule({
  declarations: [CrmComponent, SidBarComponent, ProfileComponent, TestEligibiliteComponent, MapFrenchComponent],
  imports: [
    CommonModule,
    CrmRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    CountdownModule,
    AngularMultiSelectModule,
    MultiSelectAllModule,
    NgSelectModule,
  ]
})
export class CrmModule { }
