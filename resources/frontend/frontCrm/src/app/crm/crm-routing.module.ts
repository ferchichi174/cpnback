import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrmComponent } from './crm.component';
import{ProfileComponent} from './profile/profile.component'
import{TestEligibiliteComponent} from './test-eligibilite/test-eligibilite.component'

const routes: Routes = [

  { path: '', component: CrmComponent 
,children:
[
  { path: 'profile', component: ProfileComponent },
  { path: 'test', component: TestEligibiliteComponent },
  { path: 'commercial', loadChildren: () => import('../crm/commercial/commercial.module').then(m => m.CommercialModule) },
  { path: 'supervisor', loadChildren: () => import('../crm/supervisor/supervisor.module').then(m => m.SupervisorModule) },

]},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrmRoutingModule { }
