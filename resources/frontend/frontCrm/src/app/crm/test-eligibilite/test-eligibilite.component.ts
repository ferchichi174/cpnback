import { Component, Input, NgModule, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
  import{TestEligibiliteService} from 'src/app/services/crm/test-eligibilite.service'
declare let $: any;
import { NgOption, NgSelectConfig} from '@ng-select/ng-select';
import Swal from 'sweetalert2';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}




// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports


// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};
@Component({
  selector: 'app-test-eligibilite',
  templateUrl: './test-eligibilite.component.html',
  styleUrls: ['./test-eligibilite.component.css'],
 
})
export class TestEligibiliteComponent implements OnInit {
   /***********************all variable ******************/
   elegible:any[]
   graphic:any[]
   montage:any[]
   development:any[]
   marketing:any[]
   transition:any[]
   matcher = new MyErrorStateMatcher();
  codePFormGroup: FormGroup;
  testStart:boolean=false
  time:any
  activities:any
  date = new Date('2019');
  etat:any=["credit d'impo","fond de salarite","chaumage partiel"]
  personne:any=["de 0 à 5 Personnes","de 5 à 10 Personnes","de 10 à 20 Personnes","de 20 à 30 Personnes"]
  public fields: Object = { text: 'name', value: 'id' };
  startDate = new Date(1990, 0, 1);
  i:number=1 
  nbPersonne:String=''
  haveSite:string='non';
  haveCrm:string='non';
  status: string[] = ['SARL', 'SAS', 'SASU', 'EURL', 'MICRO-ENT', 'ARTISAN', 'INDIVIDUELLE'];
/***************************************************get title by step *****************************/
 getStep(i){
   let title:string
   this.test.data.filter(data=>{ if(data.step===i){
    console.log('title',data.title)
     title = data.title
  }})
  console.log('title',title)

  return title
 }
 /*****************************************************get title by sub step ***********************/
 
 getSubStep(i){
  let title:string
  
console.log('title',title)

 return title
}
 /***********************life cycle ******************/
 clicked(){console.log('clicl',this.codePFormGroup.get('status').value)}
 onChange = ($event: any): void => {
  // console.log($event);
   console.log(`SELECTION CHANGED INTO ${$event.name || ''}`);
 }

 openModal(){
  $('#testEgib').appendTo("body").modal('show')
 }
 get f() { return this.codePFormGroup.controls; }
  constructor(private _formBuilder: FormBuilder,private testService:TestEligibiliteService) { }

  onSubmit(){}
  ngOnInit() {
    this.codePFormGroup = this._formBuilder.group({
      codeP: ['',[ Validators.required,Validators.pattern("[0-9 ]{5}")]],
      nomSoc: ['', Validators.required],
      activite: ['', Validators.required],
      status: ['', Validators.required],
      etat: ['', Validators.required],
      personneSal: ['', Validators.required],
      turnover:['error', Validators.required],
      lastTurnover:['error', Validators.required],
      helpSoc:['', Validators.required],
      liensite: ['', Validators.required],
      datesite: ['', Validators.required],
      siteVal: ['', Validators.required],
      dateCrm: ['', Validators.required],
      typeCRM: ['', Validators.required],
      typeERP: ['', Validators.required],
      typeSite: ['', Validators.required],
      crmDev: ['', Validators.required],
      agence: ['', Validators.required],
      buget: ['', Validators.required],
      service: ['', Validators.required],
      siret: ['', [ Validators.required,Validators.pattern("[0-9 ]{14}")]],
      siren: [''],
      naf: ['', Validators.required],
      adresse: ['', Validators.required],
      zipcode: ['',[ Validators.required,Validators.pattern("[0-9 ]{5}")]],
      region: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      prenom: ['', Validators.required],
      nom: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      phoneEntrep: ['', Validators.required],
      post: ['', Validators.required],
 
    })
    

    this.getActivite()
    this.getTransition()
  }

       /***********************progres test update ******************/
  progressUpdate(){  
    var progressBar = document.getElementById('progressBar');
    let progressVal = Math.round((this.test.active.step/(this.test.data.length-1))*100);
    progressBar.style.width = progressVal+"%";
}
     /***********************commencer test ******************/
  commencerTest(){
          this.testStart=true
          this.checkForm(0)
          this.openModal()
  }

  triggerFunction() {
    console.log('Timer Ended');
  }

       /***********************fermer test ******************/
 fermer(){
   this.testStart=false
 }

  /***********************select transition ******************/
  getTransition(){
    this.testService.getTransitions().subscribe(res=>{
     this.transition=  res.data
     console.log("transition",this.transition)
       this.elegible=res.data.filter(data=>data.category==='Services')
       this.graphic=res.data.filter(data=>data.category==='Graphique')
       this.montage=res.data.filter(data=>data.category==='Montage')
       this.marketing=res.data.filter(data=>data.category==='Marketing')
       this.development=res.data.filter(data=>data.category==='Développement')
       console.log("elegi",this.elegible)
       console.log("suplimet",this.development)
   })
 }
 
   /***********************select status juredique ******************/
   getActivite(){
   this.testService.getActivites().subscribe(res=>{
    this.activities=  res
      console.log("activi",this.activities)
  })
}


 /***********************select status juredique ******************/
 getSalaraie(){
  
}

/***************************get siret **************************/
onKey(value: any) {
  console.log('siret',value)
  this.codePFormGroup.get('siren').setValue(value)

}
/***************************check service**************************/
check(value: any) {
  console.log('checked',value)
  this.codePFormGroup.get('service').setValue(value)

}
/***********************perdu du chiffre d'affaires  ******************/
   getTurnover(value: number | null) {
     console.log('slider',value)
     if (!value) {
      return 0;
    }
    if (value >= 100) {
      return Math.round(value /1) + '%';
    }
    if (value ) {
      
      //this.codePFormGroup.get('turnover').setValue(value);
    }
    return value;
  }

  /***********************perdu du chiffre d'affaires  ******************/
  getLastTurnover(value: number | null) {
    console.log('slider',value)
   // this.codePFormGroup.get('lastTurnover').setValue(value)
   if (!value) {
     return 0;
   }
   if (value >= 100) {
     return Math.round(value /1) + '%';
   }
   return value;
 }

   /***********************Budget d'investissement ******************/
   getBudget(value: number | null) {
    console.log('slider',value)
   if (!value) {
     this.codePFormGroup.get('budget').setValue(value)
     return 0;
   }

   if (value >= 100) {
     return Math.round(value /1) + '%';
   }

   return value;
 }
/******************************************nextmodule  *************************************************/
nextStep(){
  this.test.active.step+=1
}

nextSubStep(){
  this.test.active.subStep+=1
}
 /******************************************prevmodule  *************************************************/
 prevStep(){
  this.test.active.step-=1;
}
/********************************************regionalGrant ****************************************/
getRegionalGrant(region,budget,naf){

    this.testService.regionalGrant(region,budget,naf).subscribe(response=>{
      if(response.data.eligible){
        this.test.result.regional.id = response.data.id;
        this.test.result.regional.eligible = response.data.eligible;
        this.test.result.regional.voucher = response.data.voucher;
        this.test.result.regional.amount = response.data.amount;
        this.test.result.regional.region = response.data.region;
      } else {
        this.test.result.regional.eligible = response.data.eligible;
        this.test.result.regional.voucher = null;
        this.test.result.regional.amount = null;
      }
      this.test.result.isLoading = false;
    })
}
/********************************************cpn grant ****************************************/
getCpnGrant(service,budget){

  this.testService.cpnGrant(service,budget).subscribe(response=>{
    this.test.result.cpn.id = response.data.id;
    this.test.result.cpn.amount = response.data.grants;
    this.test.result.cpn.originalPrice = response.data.original_price;
    this.test.result.cpn.sellPrice = response.data.sell_price;
    this.test.result.isLoading = false;
  })
}
/********************************************setContactForm ****************************************/
setContactForm(formData){

  this.testService.addContact(JSON.stringify(formData))
  .subscribe(response=>{
    if(!response.data.error){
      this.test.formData.contactID = response.data.cid;
    }
  })
} 
/******************************************show resultat  *************************************************/
 showResult(){
  let budget = this.test.formData.investment.budget;
  let service = this.test.formData.investment.service;
  let region = this.test.formData.address.region;
  let naf = this.test.formData.companies.naf;
  switch (this.test.result.isOpen) {
    case true:
      if(this.test.result.isCpn){
        this.test.result.isCpn = false;
        this.test.result.isLoading = true;
        this.getRegionalGrant(region,budget,naf)
      } else {
        this.setContactForm(this.test.formData);
        this.test.result.isCpn = true;
        this.test.result.isOpen = false;
        this.nextStep();
      }
      break;
    case false:
      this.test.result.isOpen = true;
      this.test.result.isLoading = true;
      this.getCpnGrant(service,budget)
      break;
  }
}
 /********************************************************status**********************************/
 getstatus(data){
   console.log('activi',data)
   this.codePFormGroup.get('status').setValue(data)
   
 }
 /********************************************************TypeCrm**********************************/
 getTypeCrm(data){
  console.log('typeCRM',data)
  this.codePFormGroup.get('typeCRM').setValue(data)

}
/**************************************************************have a site **************************/
getSite(data){
if(data=='OUI'){
  this.haveSite='oui'
}else{
  this.haveSite='non'
}
}
getCrm(data){
  if(data=='OUI'){
    this.haveCrm='oui'
  }
  else{this.haveCrm='non'}
  }
getTypeSite(data){
  
  this.codePFormGroup.get('typeSite').setValue(data)

}
getCrmDev(data){
  this.codePFormGroup.get('crmDev').setValue(data)

}

/******************************************************chekform **************************************/

checkForm(step){
  console.log('step',step)
  let subStep = this.test.active.subStep;
  let subStepCat = this.test.active.subStepCat;
  switch (step) {
    case 0:
      this.nextStep();
      break;
    case 1:
      if( this.codePFormGroup.get('codeP').value==''){
        return 'champ obligatoire'
      }else{this.nextStep();}
      break;
    case 2:
      if( this.codePFormGroup.get('nomSoc').value==''){
        return 'champ obligatoire'
      }else{this.nextStep();}
      break;
    case 3:
      if( this.codePFormGroup.get('status').value==''){
        return 'champ obligatoire'
      }else{this.nextStep();}
      break;
    case 4:
      if( this.codePFormGroup.get('activite').value==''){
        return 'champ obligatoire'
      }else{this.nextStep();}
      break;
    case 5:
      if( this.codePFormGroup.get('turnover').value==''){
        return 'champ obligatoire'
      }else{this.nextStep();}
      break;
    case 6:
      if( this.codePFormGroup.get('etat').value==''){
        return 'champ obligatoire'
      }else{this.nextStep();}
      break;
    case 7:
      if( this.codePFormGroup.get('lastTurnover').value==''){
        return 'champ obligatoire'
      }else{this.nextStep();}
      break;
    case 8:
     
     
        if(this.codePFormGroup.value.personneSal == "de 0 à 5 Personnes" || this.codePFormGroup.value.personneSal == "de 5 à 10 Personnes"){
          this.test.active.subStepCat = 1;
          this.test.active.subStep = 1;
          this.nextStep();
        } else {
          this.test.active.subStepCat = 2;
          this.test.active.subStep = 1;
          this.nextStep();
        }
     
      break;
    case 9:
      switch (subStepCat) {
        case 1:
          switch (subStep) {
            case 1:
            
           if(this.haveSite == "oui"){
                this.nextSubStep();
              } else {
                this.nextStep();
              };
              break;
            case 2:
             
              this.nextSubStep();
              break;
            case 3:
              this.nextSubStep();
              break;
            case 4:
              this.nextSubStep();
              break;
            case 5:
              this.nextStep();
              break;
          }
          break;
        case 2:
          switch (subStep) {
            case 1:
            
               if(this.haveCrm == "oui"){
                this.nextSubStep();
              } else {
                this.test.active.subStepCat = 1;
              };
              break;
            case 2:
              this.nextSubStep();
              break;
            case 3:
               this.nextSubStep();
              break;
            case 4:
              this.nextSubStep();
              break;
            case 5:
              this.nextSubStep();
              break;
            case 6:
             
                this.test.active.subStepCat = 1;
                this.test.active.subStep = 1;
             
              break;
          }
          break;
      }
      break;
    case 10:
     this.nextStep();
      break;
    case 11:
      this.nextStep();
      break;
    case 12:
    this.nextStep();
      break;
    case 13:
      this.nextStep();
      break;
    case 14:
    this.nextStep();
    break;
    case 15:
    this.nextStep();
    break;
    case 16:
    this.nextStep();
        
    break;
  }
}


    test:any={
      active:{
        step:0,
        subStep:1,
        subStepCat:0,
        stepType:"form",
        popup:false,
        confirmed:false,
      },
      result:{
        isOpen:false,
        isLoading:false,
        isCpn:true,
        regional:{
          id:null,
          region:null,
          eligible:false,
          voucher:null,
          amount:null,
        },
        cpn:{
          id:null,
          amount:null,
          originalPrice:null,
          sellPrice:null,
        },
      },
      zoom:{
        generating:false,
        generated:false,
      },
      formData:{
        advisorName:'',
        contactID:null,
        meetingType:null,
        address:{
          line:null,
          zipcode:null,
          region:null,
          departement:null,
          city:null,
          country:null,
        },
        contacts:{
          firstName:null,
          lastName:null,
          email:null,
          phone:null,
          position:null,
          type:3,
          comment:null,
        },
        companies:{
          name:null,
          status:null,
          activity:null,
          help:null,
          salaries:null,
          siret:null,
          siren:null,
          naf:null,
          phone:null,
          turnover:0,
          lastTurnover:null,
        },
        development:{
          haveWebsite:null,
          websiteType:null,
          websiteValue:null,
          websiteLink:null,
          websiteDate:null,
          haveCrm:null,
          crmType:null,
          crmDev:null,
          crmName:null,
          erpName:null,
          crmDate:null,
          agencyName:null,
        },
        investment:{
          service:null,
          budget:0,
          digitalTransitions:[],
        },
      },
      orientations:[],
      data:[
        {
          step:0,
          title:"Bienvenue"
        },
        {
          step: 1,
          title:"Renseigner le code postal",
        },
        {
          step:2,
          title:"Nom de l'entreprise"
        },
        {
          step:3,
          title:"Statut juridique"
        },
        {
          step:4,
          title:"Secteur d'activité",
          options:[],
        },
        {
          step:5,
          title:"Avez vous perdu du chiffre d'affaires pendant la crise sanitaire",
          labels:[
            "Baisse",
            "",
            "",
            "",
            "",
            "-50%",
            "",
            "",
            "",
            "",
            "Stable",
            "",
            "",
            "",
            "",
            "50%",
            "",
            "",
            "",
            "",
            "Hausse"
          ],
        },
        {
          step:6,
          title:"Avez vous déja obtenu des aides de l'état",
          options:[
            "Chéque numérique et aide numérique de votre région",
            "Crédit d'impôt",
            "Fond de solidarité",
            "Chaumage partiel",
            "Aucune aide",
          ],
        },
        {
          step:7,
          title:"Dernier chiffre d'affaires réalisé",
          labels:[
            "5k €",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "700k €",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "3.5m €",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "7.5m €",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "30m €",
          ],
          selectedRange:[0,1],
          range:[
            5000,
            50000,
            100000,
            200000,
            300000,
            400000,
            500000,
            600000,
            700000,
            800000,
            900000,
            1000000,
            1500000,
            2000000,
            2500000,
            3000000,
            3500000,
            4000000,
            4500000,
            5000000,
            5500000,
            6000000,
            6500000,
            7000000,
            7500000,
            8000000,
            8500000,
            9000000,
            9500000,
            10000000,
            15000000,
            20000000,
            25000000,
            30000000,
          ],
        },
        {
          step:8,
          title:"Nombre de salariés",
          options:[
            "de 0 à 5 Personnes",
            "de 5 à 10 Personnes",
            "de 10 à 20 Personnes",
            "de 20 à 30 Personnes",
            "de 30 à 40 Personnes",
            "de 40 à 50 Personnes",
            "plus de 50 Personnes",
          ],
        },
        {
          step:9,
          title:"Type de site",
          website:[
            {
              subStep:0,
              title:""
            },
            {
              subStep:1,
              title:"Avez vous un site internet pour votre entreprise"
            },
            {
              subStep:2,
              title:"Type de site"
            },
            {
              subStep:3,
              title:"Lien de site"
            },
            {
              subStep:4,
              title:"Date de développement"
            },
            {
              subStep:5,
              title:"L'agence qui a développé votre site"
            }
          ],
          crm:[
            {
              subStep:0,
              title:""
            },
            {
              subStep:1,
              title:"Avez vous un crm pour votre entreprise"
            },
            {
              subStep:2,
              title:"Type de crm"
            },
            {
              subStep:3,
              title:"Le crm a été développé"
            },
            {
              subStep:4,
              title:"Quel type de ERP vous utilisez",
              options:[
                "Zoho",
                "SAP",
                "Sage",
                "Oracle",
                "NetSuite",
                "Cegid",
                "Microsoft Dynamics",
                "Divalto",
                "WaveSoft",
                "Odoo",
                "Archipelia",
                "Axonaut",

              ]
            },
            {
              subStep:5,
              title:"Quel type de CRM vous utilisez",
              options:[
                "Zoho",
                "Habspot",
                "Microsoft Dynamics",
                "SalesForce",
                "Pipedrive",
                "Agile",
                "Axonaut",
                "FreshSales",
                "Teamleader",
                "Facture",
                "Crème",
                "Suite",
              ]
            },
            {
              subStep:6,
              title:"Date de développement"
            }
          ]
        },
        {
          step:10,
          title:"Quel projet est à subventionner pour votre transition numérique",
          tabServices:null,
          loading:false,
          services:["Services éligible", "Services suplémentaire"],
          tabCategories:null,
          categories:["Tous", "Graphique", "Développement","Montage","Marketing"],
          options:[],
        },
        {
          step:11,
          title:"Budget d'investissement",
          budget:5,
          min:400,
          target:500,
          max:100000,
        },
        {
          step:12,
          title:"Numéros d'identification",
          loading:false,
        },
        {
          step:13,
          title:"Adresse",
        },
        {
          step:14,
          title:"Fiche de renseignement",
          options:[
            "Gérant",
            "Directeur",
            "Associé",
            "Autre"
          ],
        },
        {
          step:15,
          title:"Vos disponibilités",
        },
        {
          step:16,
          title:"Type de client",
          items: ['☹️', '🙁', '😐', '🙂', '😊', '😍'],
          labels:[
            "agressif",
            "indécis",
            "anxieux",
            "économe",
            "compréhensif",
            "roi",
          ]
        },
        {
          step:17,
          title:"Merci pour votre temps",
        },
      ],
    }
  

}
