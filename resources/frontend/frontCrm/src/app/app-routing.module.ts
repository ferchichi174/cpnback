import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MapFrenchComponent } from './map-french/map-french.component';
import {LoginComponent} from './login/login.component'
import {TestComponent} from './test/test.component'
import {HomeComponent} from './home/home.component'

const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'test', component: TestComponent },
  { path: 'map', component: MapFrenchComponent },
  { path: 'crm', loadChildren: () => import('./crm/crm.module').then(m => m.CrmModule) },
  { path: 'cpn', loadChildren: () => import('./cpn/cpn.module').then(m => m.CpnModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
