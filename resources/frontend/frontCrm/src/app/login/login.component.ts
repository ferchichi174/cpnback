import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Profile } from '../models/crm-models/profile';
import {AuthService} from '../services/auth.service';
import { TokenStorageService} from '../services/token-storage.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /*********************all variable **********************/
  LoginForm: FormGroup;

  /**********************life Cycle ***********************/
  constructor(private fb: FormBuilder,private router: Router, private _Activatedroute: ActivatedRoute, private auth:AuthService,
    private tokenStorage:TokenStorageService) {
    this.LoginForm = this.fb.group({
      email: [null,[ Validators.required]],
      password: ['', [Validators.required]],

    });
   }

  ngOnInit(): void {
  }

  /************************login *************************/
  onSubmit(){
    this.auth.login(this.LoginForm.value.email,this.LoginForm.value.password).subscribe(data=>{
       this.tokenStorage.saveToken(data.token);
         this.tokenStorage.saveUser(data.user);
         location.href='/crm/profile'
    })

  }

}
