<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class usersocial extends Model
{
    use HasFactory;
    protected $fillable = [
        "id",
        "user_id",
        "twitter",
        "instagram",
        "facebook",
        "teleph",
    ];
}
