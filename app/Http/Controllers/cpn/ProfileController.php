<?php

namespace App\Http\Controllers\cpn;

use App\Http\Controllers\Controller;
use App\Models\Addresses;
use App\Models\Picture;
use App\Models\User;
use App\Models\usersocial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function profile (Request $request){
        try {
     $user_id = $request->user()->id;
     $data = User::find($user_id);
     $adresse = Addresses::where('users_id', Auth()->user()->id)->get()->first();
     $avatar = Picture::where('users_id', Auth()->user()->id)->get()->first();
     $usersocial = usersocial::where('user_id', Auth()->user()->id)->get()->first();

            return response()->json(["status"=>true,"message"=>"User profile",'data'=>$data,'adress'=>$adresse,'image'=>$avatar,'social'=>$usersocial]);
    } catch (\Exception $e){
            return response()->json(["status"=>false,"message"=>$e->getMessage(),'data'=>[]],500);
        }
    }

    public function update_profile(Request $request){
  try {
        $validator = Validator::make($request->all(),[
            "firstname" => 'required|min:3|max:45',
            "adresse" => 'required|min:3|max:60',
            "lastname" => 'required|min:3|max:45',
            "email"=> 'required|email|unique:users,id,'.$request->user()->id,

            ],);
        if($validator->fails()) {
            return response()->json(["error"=>true,"message"=>$validator->errors()->first()]);}
    else {
    $user = User::find($request->user()->id);
    $user->first_name = $request->firstname;
    $user->last_name = $request->lastname;
    $user->email = $request->email;
    $user->update();
    $adresse = Addresses::where('users_id', $request->user()->id)->get()->first();

    $adresse->address = $request->adresse;
    $adresse->update();

        $usersocial = usersocial::where('user_id', $request->user()->id)->get()->first();
        if ($usersocial == null){
            $userso=  usersocial::create([
            'user_id'=>$request->user()->id,
            'twitter' => $request->twitter,
            'instagram'=> $request->instagram,
            'facebook'=> $request->facebook,
            'teleph' => $request->teleph,

        ]);

        }
        else{

            $usersocial = usersocial::where('user_id', $request->user()->id)->get()->first();
        $usersocial->twitter = $request->twitter;
        $usersocial->instagram = $request->instagram;
        $usersocial->facebook = $request->facebook;
        $usersocial->teleph = $request->teleph;
        $usersocial->update();}

        if ($request->hasFile('image')) {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('images', $filename, 'public');
            $path = public_path('img/');
            $request->image->move($path,$filename);
            $avatar = Picture::where('users_id', Auth()->user()->id)->get()->first()->update(['link' => $filename]);}


        return response()->json(["status"=>true,"message"=>"profile updated",'data'=>$user,$adresse,$usersocial ]);
    }
  }catch (\Exception $e){
      return response()->json(["status"=>false,"message"=>$e->getMessage(),'data'=>[]],500);
  }
}
}
