<?php
//
//namespace App\Http\Controllers\WEB;
//
//use App\Models\User;
//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
//use phpDocumentor\Reflection\Types\Self_;
//use App\Http\Controllers\Controller;



namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;

use App\Http\Requests\Auth\LoginRequest;

use App\Models\Addresses;
use App\Models\Social_account;
use App\Mail\SignupEmail;
use App\Models\User;
use App\Models\Picture;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Self_;

class RegisterController extends Controller
{


//
    public function trait_reg(Request $request)
    {
        $rules =    [
            'email' => 'required | unique:users,email',
            'password' => 'required|min:8',
            'password_confirmed' => 'required | same:password',
            'type' => 'required'
        ];

        $messages =       [
            'email.required' => 'Ce champ est obligatoire .',
            'email.unique' => 'Cette email est existe déja .',
            "email.email" => "Email n'est pas valide .",
            "password.required" => "Mot de passe est requis .",
            "password_confirmed.required" =>'La confirmation du mot de passe est requis .',
            'password_confirmed.same' => 'La confirmation du mot de passe doit correspondre au mot de passe .',
            'password.min' => 'Pour des raisons de sécurité, votre mot de passe doit faire :min caractères.',
            'type.required' => "Il faut choisir votre type d'entreprise!."
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) return response()->json(["error"=>true,"message"=>$validator->errors()->first()]);

        $user = User::create([

            'last_name'=> $request->lastname,
            'first_name'=> $request->firstname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'type' => $request->type,
        ]);

        event(new Registered($user));

        $token = $user->createToken('authtoken');

//
//            $adresse = new Addresses();
//            $adresse->users_id = $user->id;
//            $adresse->save();
//            $picture = new Picture();
//            $picture->users_id = $user->id;
//            $picture->link = "https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg";
//            $picture->save();

        $adresse = new Addresses();
        $adresse->users_id = $user->id;
        $adresse->save();
        $picture = new Picture();
        $picture->users_id = $user->id;
        $picture->link = null;
        $picture->save();


        return response()->json(
            [
                'message'=>'User Registered',
                'data'=> ['token' => $token->plainTextToken, 'user' => $user]
            ]
        );

            //        $social_accounts=new Social_account();
            //        $social_accounts->user_id=$user->id;
            //        $social_accounts->save();

        //        $social_accounts=new Social_account();
        //        $social_accounts->user_id=$user->id;
        //        $social_accounts->save();



//                $user->sendEmailVerificationNotification();



            }
//    public function login(LoginRequest $request)
//    {
////        $loginData = $request->validate([
////            'email' => 'email|required',
////            'password' => 'required'
////        ]);
//
////        if(!auth()->attempt($loginData)) {
////            return response(['message'=>'Invalid credentials']);
////        }
//
////        $accessToken = auth()->user()->createToken('authToken')->accessToken;
//
//    }
    public function login(LoginRequest $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if(!auth()->attempt($loginData)) {
            return response(['message'=>'Invalid credentials']);
        }

     /*   if(!Auth::user()->email_verified_at) {
            return response()->json(["status"=>false,"message"=>"Email not verified!"]);
        }
*/
        $request->authenticate();


        $token = $request->user()->createToken('authtoken');
        return response()->json(
            [
                'message'=>'Logged in baby',
                'data'=> [
                    'user'=> $request->user(),
                    'token'=> $token->plainTextToken
                ]
            ]
        );

    }


    public function logout(Request $request)
    {

        $request->user()->tokens()->delete();

        return response()->json(
            [
                'message' => 'Logged out'
            ]

        );

    }


}
